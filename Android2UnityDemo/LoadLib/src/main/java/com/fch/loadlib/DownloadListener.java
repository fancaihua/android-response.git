package com.fch.loadlib;

public interface DownloadListener {

    void onProgress(String name, int progress);

    void onDownloaded(String name);

    void onError(String name, String message);
}


