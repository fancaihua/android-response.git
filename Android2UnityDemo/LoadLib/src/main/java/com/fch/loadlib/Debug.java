package com.fch.loadlib;

import android.util.Log;

import com.unity3d.player.UnityPlayer;

public class Debug {

    public static void Log(String source, String message) {
        Log.d(source,message);
        UnityPlayer.UnitySendMessage("Log", "ShowLog", source + ":" + message);
    }
}
