package com.fch.loadlib;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.FileProvider;

import com.unity3d.player.UnityPlayer;

import java.io.File;

public class MainActivity extends Activity {

    private static final int REQUEST_CODE_STORAGE_PERMISSION = 1234;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.d("LoadLib", "MainActivity！");

        requestStoragePermission();
    }


    //-----------安装APK-------------

    // 安装APK包
    private void installApk() {

        File file = new File("/storage/emulated/0/CreateParty/Install/DJ_Install.apk");
        Intent intent = new Intent(Intent.ACTION_VIEW);
        Uri uri = FileProvider.getUriForFile(this, "com.fch.loadlib.fileprovider", file);
        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.setDataAndType(uri, "application/vnd.android.package-archive");
        startActivity(intent);
    }


    public void requestStoragePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_CODE_STORAGE_PERMISSION);
        } else {
            // 如果系统版本低于 6.0，则默认具有存储权限
            installApk();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE_STORAGE_PERMISSION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // 如果用户授予了存储权限，则开始下载 APK 文件
                installApk();
            } else {
                // 如果用户拒绝了存储权限，则提示用户
                Toast.makeText(this, "存储权限被拒绝", Toast.LENGTH_SHORT).show();
            }
        }
    }

    //---------------------------


    public int Add(int a, int b) { return a + b; }

    public String CallAndroidMessage(String unityString) {
        unityString = unityString + "yong.love";
        UnityPlayer.UnitySendMessage("Canvas", "ShowMessage", unityString);
        return "安卓信息:我收到了信息，" + unityString;
    }

    public MainActivity() { }
    public MainActivity(String defaultDirectory) {this.defaultDirectory = defaultDirectory;}

    private String defaultDirectory;
    private String fileURL;
    private String fileName;
    public void Download(String url, String name) {
        fileURL = url;
        fileName = name;

        Debug.Log("MainActivity:", "DownLoadStart");
    }

    public void SetDownloadListener(DownloadListener listener) {

        Debug.Log("MainActivity:", "SetDownloadListener");

        if (listener != null) {
            listener.onProgress("下载进度：", 38);
            listener.onDownloaded("下载完成!" + fileURL + fileName);
            listener.onError("thisName:", "错误信息");

            requestStoragePermission();
        }
    }


    //-------------创建文件夹--------------

    /*

    // 创建对话框请求用户授权
    private void requestStoragePermission2() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            try {
                Intent intent = new Intent(Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION);
                intent.addCategory("android.intent.category.DEFAULT");
                intent.setData(Uri.parse("package:" + getPackageName()));
                startActivityForResult(intent, REQUEST_CODE_STORAGE_PERMISSION);
            } catch (Exception e) {
                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION);
                startActivityForResult(intent, REQUEST_CODE_STORAGE_PERMISSION);
            }
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CODE_STORAGE_PERMISSION);
        }
    }

    // 在授权成功后创建文件
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE_STORAGE_PERMISSION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                ProDownLoad proDownLoad = new ProDownLoad(MainActivity.this);
                proDownLoad.StartLoad();
            }
        }
    }

    */

    //---------------------------




}
