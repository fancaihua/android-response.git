package com.fch.loadlib;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import com.unity3d.player.UnityPlayer;

import java.util.LinkedList;
import java.util.Queue;

public class UnityBroadcastHelper {
    private static final String TAG = "UnityBroadcastHelper";
    public interface BroadcastListener {
        void onReceive(String action);
    }
    private final BroadcastListener listener;
    private Queue<String[]> keysQueue = new LinkedList<>();
    private Queue<String[]> valuesQueue = new LinkedList<>();
    public UnityBroadcastHelper(String[] actions, BroadcastListener listener) {
        Debug.Log(TAG, "UnityBroadcastHelper: actions: " + actions);
        Debug.Log(TAG, "UnityBroadcastHelper: listener: " + listener);
        this.listener = listener;
        IntentFilter intentFilter = new IntentFilter();
        for (String action : actions) {
            intentFilter.addAction(action);
        }
        Context context = UnityPlayer.currentActivity;
        if (context == null) {
            return;
        }
        context.registerReceiver(broadcastReceiver, intentFilter);
    }
    public boolean hasKeyValue() {
        return !keysQueue.isEmpty();
    }
    public String[] getKeys() {
        return keysQueue.peek();
    }
    public String[] getValues() {
        return valuesQueue.peek();
    }
    public void pop() {
        keysQueue.poll();
        valuesQueue.poll();
    }
    public void stop() {
        Context context = UnityPlayer.currentActivity;
        if (context == null) {
            return;
        }
        context.unregisterReceiver(broadcastReceiver);
    }
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Debug.Log(TAG, "UnityBroadcastHelper: action: " + action);
            Bundle bundle = intent.getExtras();
            if (bundle == null) {
                bundle = new Bundle();
            }
            int n = bundle.size();
            String[] keys = new String[n];
            String[] values = new String[n];
            int i = 0;
            for (String key : bundle.keySet()) {
                keys[i] = key;
                Object value = bundle.get(key);
                values[i] = value != null ? value.toString() : null;
                Debug.Log(TAG, "UnityBroadcastHelper: key[" + i + "]: " + key);
                Debug.Log(TAG, "UnityBroadcastHelper: value[" + i + "]: " + value);
                i++;
            }

            keysQueue.offer(keys);
            valuesQueue.offer(values);
            listener.onReceive(action);
        }
    };
}